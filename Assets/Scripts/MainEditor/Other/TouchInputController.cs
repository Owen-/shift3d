﻿using UnityEngine;
using System.Collections;

public class TouchInputController
{
	public enum Type { Directional, Positional } // dragging vs selecting
	private Type type;

	public delegate void TouchOperation(float x, float y);
	private TouchOperation onDown;
	private TouchOperation onHold;
	private TouchOperation onUp;

	public TouchInputController()
	{
		onDown = null;
		onHold = null;
		onUp = null;
		type = Type.Directional;
	}

	public void Initialize(TouchOperation onDown, TouchOperation onHold, TouchOperation onUp, Type type)
	{
		this.onDown = onDown;
		this.onHold = onHold;
		this.onUp = onUp;
		this.type = type;
	}

	public void TryPerformOperation()
	{
		#if UNITY_EDITOR // quick fix to give me access to testing these controls in the unity editor
			if(Input.GetKey(KeyCode.Mouse0) && !RaycastManager.isOverUI)
				PerformOperationUnityEditor();
		#endif

		#if UNITY_ANDROID
			if (Input.touchCount == 1 && !RaycastManager.isOverUI) 
				PerformOperationAndroid();
		#endif
	}

	#if UNITY_EDITOR
	public void PerformOperationUnityEditor()
	{
		float x = (type==Type.Positional) ? Input.mousePosition.x : Input.GetAxis("Mouse X");
		float y = (type==Type.Positional) ? Input.mousePosition.y : Input.GetAxis("Mouse Y");

		if 		(Input.GetKeyDown(KeyCode.Mouse0))	
		{
			if (onDown != null) 
				onDown(x,y);
		}

		else if (Input.GetKeyUp(KeyCode.Mouse0)) 	
		{
			if (onUp != null) 
				onUp(x,y);
		}

		else if (Input.GetKey(KeyCode.Mouse0)) 		
		{
			if (onHold != null) 
				onHold(x,y);			
		}
	}
	#endif
	
	#if UNITY_ANDROID	
	public void PerformOperationAndroid()
	{
		Touch T = Input.GetTouch(0);
		float x = (type==Type.Positional) ? T.position.x : T.deltaPosition.x;
		float y = (type==Type.Positional) ? T.position.y : T.deltaPosition.y;

		if 		(T.phase==TouchPhase.Began)	
		{
			if (onDown!=null) onDown(x,y);
		}

		else if (T.phase==TouchPhase.Ended) 
		{
			if (onUp!=null) onUp(x,y);
		}

		else  				
		{				
			if (onHold!=null) onHold(x,y);
		}
	}
	#endif		
}
