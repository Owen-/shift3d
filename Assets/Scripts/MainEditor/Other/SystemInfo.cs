﻿using UnityEngine;
using System.Collections;

public static class SystemInfo 
{
	public static bool isRunningOnAndroid 
	{
		get 
		{
			if (Application.platform == RuntimePlatform.Android) return true;
			else return false;
		} 
	} 
}
