using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//remove usings
//make namespace - can you use namespace instead of class??? that would explain a lot and be cool
//make list class that loops around when you hit count/ count +1 or just extension metho

public static class TopologyGenerator3D
{
    //Takes in a list of vertices that form the perimeter of a face and generates triangles indices for a meshrenderer
    public static int[] GenerateTris(List<Vector3> points3D)
    {
        List<int> indices = new List<int>();
        for (int i = 0; i<points3D.Count; i++ ) indices.Add(i);

        return GenerateTrisRecursive(points3D, indices, new List<int>());
    }

	// Recursively calculates a triangle at the corner of the shape's perimeter
	// Once triangle is found it is removed from the perimeter and the process repeats
    private static int[] GenerateTrisRecursive(List<Vector3> points3D, List<int> indices, List<int> result)
    {
    	Vector3 faceNormal = FaceNormal(points3D);

        if (indices.Count >=3)
        {
            for (int i = 0; i<indices.Count; i++)
            {
                int next = i+1;
                int third = i+2;

                if (i==indices.Count-2) {third = 0;}
                if (i==indices.Count-1) {next = 0; third = 1;}

				Vector3 point = points3D[indices[i]];
				Vector3 nextPoint = points3D[indices[next]];
				Vector3 thirdPoint = points3D[indices[third]];

                if (IsTriangle(point, nextPoint, thirdPoint, points3D, faceNormal))
                {
                    result.Add(indices[i]);
                    result.Add(indices[next]);
                    result.Add(indices[third]);
                    indices.RemoveAt(next);
                    return GenerateTrisRecursive(points3D, indices, result);
                }
            }          
        }
        return result.ToArray();
    }

    //averages the normal vectors of each vertex, and returns the face normal
    public static Vector3 FaceNormal(List<Vector3> points3D)
    {
        Vector3 faceNormal = new Vector3(0f,0f,0f);
        for (int i = 0; i<points3D.Count; i++)
        {
            if (i+2 < points3D.Count)         faceNormal+= NormalVector(points3D[i], points3D[i+1], points3D[i+2]);
            else if (i+2 == points3D.Count)   faceNormal+= NormalVector(points3D[0], points3D[i], points3D[i+1]);
            else if (i+1 == points3D.Count)   faceNormal+= NormalVector(points3D[0], points3D[1], points3D[i]);
        }
        faceNormal.Normalize();
        return faceNormal;
    }

    //Returns the normal vector of a point A, given 2 neighbour points
    public static Vector3 NormalVector(Vector3 A, Vector3 B, Vector3 C)
    {
		return Vector3.Cross(B-A, C-A);
    }


	// Determines if the 3 points referenced can form a triangle 
    public static bool IsTriangle(Vector3 A, Vector3 B, Vector3 C, List<Vector3> points, Vector3 faceNorm)
    {
        if (!InsideTriangle(A, B, C, points, faceNorm))
        {
            if (FacingForward(A, B, C, faceNorm))
            {
                if (IsAcuteAngle(A, B, C))
                {
                    return true;
                }
            }
        }
        return false;
    }

	//Calculates the normal of the selected points, returns true if facing the same way as the face
    public static bool FacingForward(Vector3 A, Vector3 B, Vector3 C, Vector3 norm)
    {
        if (Vector3.Dot(Vector3.Cross(B-A, B-C), norm) < 0) return true;
        return false;
    }

	//Ensure the points are not on a line 
    public static bool IsAcuteAngle(Vector3 A, Vector3 B, Vector3 C)
    {
        if (Vector3.Angle(B-A, B-C) < 180) return true;
        return false;
    }

	//Calculates if any points in a list reside within the triangle made by the points ABC
    public static bool InsideTriangle (Vector3 A, Vector3 B, Vector3 C, List<Vector3> Ps, Vector3 norm) 
    {
        foreach (Vector3 P in Ps) 
        {
            if(P!=A && P!=B && P!=C)
            {
                if (InsideTriangle(A, B, C, P, norm)) 
                {
                    return true;
                }
            }
        }
        return false;
    }


    //is P within triangle formed by ABC
    public static bool InsideTriangle (Vector3 A, Vector3 B, Vector3 C, Vector3 P, Vector3 norm) 
    {
        bool r1 = IsNotLeftOf(A, C, P, norm);
        bool r2 = IsNotLeftOf(C, B, P, norm);
        bool r3 = IsNotLeftOf(B, A, P, norm);

		if ( !r1 || !r2 || !r3 )
			return false;
		else
			return true;
    }

    //Determines which side of the line P is on
    public static bool IsNotLeftOf(Vector3 From, Vector3 To, Vector3 P, Vector3 norm)
    {
        Vector3 AB = To-From;
        Vector3 AP = To-P;

        if( Vector3.Dot(Vector3.Cross(AB, AP), norm) >= 0f) return true;
        else return false;
    }
}
	