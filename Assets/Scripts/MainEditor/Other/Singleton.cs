﻿using UnityEngine;
using System.Collections;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour 
{
	private static T instance;

	public static T Instance
	{
		get 
		{ 
			if (instance==null) instance = CreateInstance();
			return instance;
		}
	}

	private static T CreateInstance()
	{
		GameObject GO = new GameObject("Singleton");
		DontDestroyOnLoad(GO);
		return GO.AddComponent<T>();
	}

	virtual public void Start()
	{
		if (instance==null) 
		{
			instance = this as T;
			DontDestroyOnLoad(this.gameObject);
		}

		if (instance!= this as T) Destroy(this.gameObject);
	}


}