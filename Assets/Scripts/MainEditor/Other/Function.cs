﻿using System.Linq;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Still needs get highest point

public static class Function
{
	public static Vector3 GetCentrePoint(List<Point> ps)
	{
		Vector3 result = new Vector3(0f,0f,0f);
		foreach (Point P in ps) result += P.loc;
		return result/ps.Count;
	}

	public static Vector3 GetHighestPoint(List<Point> ps)
	{
		return new Vector3(0f,0f,0f);
	}

	public static Vector3 GetHighestPoint(List<Point> ps, Vector3 plane)
	{
		return new Vector3(0f,0f,0f);
	}

	public static Point ClosestPoint(Vector3 target, List<Point> ps)
    {
        float dist = Vector3.Distance(target, ps[0].loc);
        int id = 0;

        for(int i =1; i< ps.Count; i++)
        {
            if (Vector3.Distance(target, ps[i].loc) < dist)
            {
                 dist = Vector3.Distance(target, ps[i].loc);
                 id = i;
            }
        }
        return ps[id];
    }

    public static Edge ClosestEdge(Vector3 target, List<Edge> es)
    {
        float dist = Vector3.Distance(cam2Dcoord(target), 
        	ClosestV3OnLine(
        		cam2Dcoord(es[0].GetPointA().loc), 
        		cam2Dcoord(es[0].GetPointB().loc), 
        		cam2Dcoord(target))
        	);
        int id = 0;
        for (int i=1; i<es.Count; i++)
        {
            if (Vector3.Distance(cam2Dcoord(target), ClosestV3OnLine(cam2Dcoord(es[i].GetPointA().loc), cam2Dcoord(es[i].GetPointB().loc), cam2Dcoord(target))) < dist)
            {
                dist = Vector3.Distance(cam2Dcoord(target), ClosestV3OnLine(cam2Dcoord(es[i].GetPointA().loc), cam2Dcoord(es[i].GetPointB().loc), cam2Dcoord(target)));
                id = i;
            }
        }
        return es[id];

    }

    public static Vector3 ClosestV3OnLine(Vector3 vA, Vector3 vB, Vector3 vPoint)
    {
        Vector3 vVector1 = vPoint - vA;
        Vector3 vVector2 = (vB - vA).normalized;
     
        float d = Vector3.Distance(vA, vB);
        float t = Vector3.Dot(vVector2, vVector1);
     
        if (t <= 0) return vA;
     
        if (t >= d) return vB;
     
        Vector3 result = vVector2 * t;
     
        Vector3 vClosestV3 = vA + result;
     
        return vClosestV3;
    }

    public static Vector3 cam2Dcoord(Vector3 v)
    {
        v=Camera.main.WorldToScreenPoint(v);
        return new Vector3(v.x,v.y,0f);
    }

    public static List<Edge> GetEdgesFromFaces(List<Face> fs)
    {
    	List<Edge> result = new List<Edge>();
    	for(int i = 0; i<fs.Count; i++)
    	{
    		result.AddRange(fs[i].GetEdges());
    	}
    	return result;
    }

    public static List<Point> GetUniquePointsFromEdges(List<Edge> es)
    {
    	List<Point> result = new List<Point>();
    	for (int i=0; i<es.Count; i++) result.Add(es[i].GetPointA());
    	for (int i=0; i<es.Count; i++) result.Add(es[i].GetPointB());
    	return result.Distinct().ToList();
    }

    public static List<Point> GetUniquePointsFromFaces(List<Face> fs)
    {
    	List<Point> result = new List<Point>();
    	foreach (Face f in fs) result.AddRange(f.GetPoints());
    	return result.Distinct().ToList();
    }
    public static List<Vector3> GetVertsFromPoints(List<Point> ps)
    {
        List<Vector3> result = new List<Vector3>();
        foreach (Point p in ps) result.Add(p.loc);
        return result;
	}
}