﻿using UnityEngine;
using System.Collections;

public class DirectionGizmo : MonoBehaviour 
{
	public Color xC, yC, zC;
	public float lineLength, textSpacer, textSize;
	public Vector3 origin;	
	public Transform xT, yT, zT;

	private Material lineMaterial;
	private TextMesh tmX, tmY, tmZ;

	private void Start()
	{
		tmX=xT.gameObject.GetComponent<TextMesh>();
		tmY=yT.gameObject.GetComponent<TextMesh>();
		tmZ=zT.gameObject.GetComponent<TextMesh>();

		lineMaterial = new Material	(Shader.Find("S3DGizmoLines")); 
		lineMaterial.hideFlags = HideFlags.HideAndDontSave;
		lineMaterial.shader.hideFlags = HideFlags.HideAndDontSave;
	}

	private void OnRenderObject()
	{
		Vector3 pos = Camera.main.ViewportToWorldPoint(origin);
		float length = lineLength * Camera.main.orthographicSize;

		GL.PushMatrix(); 
		lineMaterial.SetPass(0);
		//GL.LoadPixelMatrix();
		GL.Begin(GL.LINES);

		GL.Color(xC); 
		GL.Vertex(pos);
		GL.Vertex(pos+Vector3.right*length);

		GL.Color(yC);
		GL.Vertex(pos);
		GL.Vertex(pos+Vector3.up*length);

		GL.Color(zC);
		GL.Vertex(pos);
		GL.Vertex(pos+Vector3.forward*length);


		GL.End();
		GL.PopMatrix();	
	
		pos = Camera.main.ViewportToWorldPoint(origin) + Camera.main.transform.right*textSpacer*Camera.main.orthographicSize;
		length = lineLength * Camera.main.orthographicSize * 1.1f;
		xT.position = pos+Vector3.right*length;
		yT.position = pos+Vector3.up*length;
		zT.position = pos+Vector3.forward*length;

		xT.LookAt(Camera.main.transform.position*-1f);
		yT.LookAt(Camera.main.transform.position*-1f);
		zT.LookAt(Camera.main.transform.position*-1f);

		tmZ.characterSize=textSize*Camera.main.orthographicSize;
		tmY.characterSize=textSize*Camera.main.orthographicSize;
		tmX.characterSize=textSize*Camera.main.orthographicSize;
	}
}
