using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//do I even need to bother with the 2d conversion?
//there is multiple ways to triangulate in 3d space
//you can still determine if something is coplanar and inside the tri

public static class TopologyGenerator
{
    //Takes in a list of Vector3s and generates the triangle information for a meshrenderer
    public static int[] GenerateTris(List<Vector3> points3D)
    {
        List<Vector2> points2D  = ProjectToXPlane(points3D);
        int[] result            = GenerateTris(points2D);

        return result;
    }

    //projects 3D points onto 2D plane (x plane)
    public static List <Vector2> ProjectToXPlane(List<Vector3> points3D)
    {
        points3D = RotateToFaceX(points3D);
        
        List<Vector2> v2s = new List<Vector2>();
    	foreach(Vector3 P in points3D) v2s.Add(P);
    	return v2s;
    }

    //Rotates normal to the +Z plane
    public static List<Vector3> RotateToFaceX(List<Vector3> points3D)
    {     
        List<Vector3> vs = new List<Vector3>();
        Vector3 normal = CalcNorm(points3D);

        for(int i=0; i<points3D.Count; i++) 
        {
            Vector3 dir = points3D[i];
            dir=Quaternion.FromToRotation(normal, Vector3.forward) * dir;
            vs.Add(dir);
        }
        return vs;
    }

    //Rotates normal to the +Z plane, and removes all curling 
    // public static List<Vector3> UnWrapToX(List<Vector3> points3D)
    // {     
    //     List<Vector3> vs = new List<Vector3>();
    //     Vector3 normal = CalcNorm(points3D);

    //     for(int i=0; i<points3D.Count; i++) 
    //     {
    //     	//if(CalcNorm(thistri) >= normal+-90)
    //     	//rotate the rest of the points around this point to face normal
    //     }
    //     return vs;
    // }

    //Calculates the normal of a set of points with regular winding.
    //Doing 3 times the calculations it needs to (consider case of 1 tri)
    //also doesnt take into account the size of the face, its auto normalised before adding
    public static Vector3 CalcNorm(List<Vector3> points3D)
    {
        Vector3 dir = new Vector3(0f,0f,0f);
        for (int i = 0; i<points3D.Count; i++)
        {
            if (i+2 < points3D.Count)         dir+= CalcNorm(points3D[i], points3D[i+1], points3D[i+2]);
            else if (i+2 == points3D.Count)   dir+= CalcNorm(points3D[i], points3D[i+1], points3D[i+2]);
            else if (i+1 == points3D.Count)   dir+= CalcNorm(points3D[i], points3D[i+1], points3D[i+2]);
        }
        dir.Normalize();
        return dir;
    }

    //Returns the normal of a Triangle
    public static Vector3 CalcNorm(Vector3 A, Vector3 B, Vector3 C)
    {
		return Vector3.Normalize(Vector3.Cross(B-A, C-A));
    }

	//Takes in a list of Vector2s and generates the triangle information for a meshrenderer
    public static int[] GenerateTris(List<Vector2> points2D)
    {
        List<int> indices = new List<int>();
        List<int> result = new List<int>();
        for (int i = 0; i<points2D.Count; i++ ) indices.Add(i);

        return GenerateTrisRecursive(points2D, indices, result);
    }

	// Recursively removes an "ear" from the perimeter until all all triangles are assigned
    private static int[] GenerateTrisRecursive(List<Vector2> points2D, List<int> indices, List<int> result)
    {
        if (indices.Count >=3)
        {
            for (int i = 0; i<indices.Count; i++)
            {
                int next = i+1;
                int third = i+2;

                if (i==indices.Count-2) {third = 0;}
                if (i==indices.Count-1) {next = 0; third = 1;}

				Vector2 point = points2D[indices[i]];
				Vector2 nextPoint = points2D[indices[next]];
				Vector2 thirdPoint = points2D[indices[third]];

                if (IsTriangle(point, nextPoint, thirdPoint, points2D))
                {
                    result.Add(indices[i]);
                    result.Add(indices[next]);
                    result.Add(indices[third]);
                    indices.RemoveAt(next);
                    return GenerateTrisRecursive(points2D, indices, result);
                }
            }          
        }
        return result.ToArray();
    }

	// Determines if the 3 points referenced can form a triangle 
    public static bool IsTriangle(Vector2 A, Vector2 B, Vector2 C, List<Vector2> points)
    {
        if (!InsideTriangle(A, B, C, points))
        {
            if (FacingForward(A, B, C))
            {
                if (IsAcuteAngle(A, B, C))
                {
                    return true;
                }
            }
        }

        return false;
    }

	//Calculates the normal of the selected points, returns true if facing the same way as the face
    public static bool FacingForward(Vector2 A, Vector2 B, Vector2 C)
    {
        if (Cross(B-A, B-C) < 0) return true;
        return false;
    }

	//Ensure the points are not on a line 
    public static bool IsAcuteAngle(Vector2 A, Vector2 B, Vector2 C)
    {
        if (Vector2.Angle(B-A, B-C) < 180) return true;
        return false;
    }

	//Calculates if any points in a list reside within the triangle made by the points ABC
    public static bool InsideTriangle (Vector2 A, Vector2 B, Vector2 C, List<Vector2> Ps) 
    {
        foreach (Vector2 P in Ps) 
        {
            if(P!=A && P!=B && P!=C)
            {
                if (InsideTriangle(A, B, C, P)) 
                {
                    return true;
                }
            }
        }
        return false;
    }


    //is P within triangle formed by ABC
    public static bool InsideTriangle (Vector2 A, Vector2 B, Vector2 C, Vector2 P) 
    {
        bool r1 = IsNotLeftOf(A, C, P);
        bool r2 = IsNotLeftOf(C, B, P);
        bool r3 = IsNotLeftOf(B, A, P);

		if ( !r1 || !r2 || !r3 )
			return false;
		else
			return true;
    }

    //Determines which side of the line P is on
    public static bool IsNotLeftOf(Vector2 From, Vector2 To, Vector2 P)
    {
        Vector2 AB = To-From;
        Vector2 AP = To-P;

        if( Cross(AB, AP) >= 0f) return true;
        else return false;
    }
    
    //Cross product of two Vector2s
    public static float Cross (Vector2 A, Vector2 B)
    {
        return (A.x)*(B.y) - (A.y)*(B.x);
    }
}
	