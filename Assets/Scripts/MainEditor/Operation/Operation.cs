using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//something here should probably
public abstract class Operation 
{
	protected Vector3 operationDirection
	{
		get
		{
			return DirectionManager.Instance.GetDirection();
		}
	}

	//this should maybe be in inpputmanager somehow and when event called
	public Operation()
	{
		//InputManager.Instance.primaryAction.SetIsDelta(true);
		//InputManager.Instance.primaryAction.onHold = VisualUpdate;
		//InputManager.Instance.primaryAction.onUp = RegisterEndState;
	}
		
	public abstract void VisualUpdate(params float[] dims);
	public abstract void RegisterEndState(params float[] dims);
	public virtual void Perform(){}
}
	