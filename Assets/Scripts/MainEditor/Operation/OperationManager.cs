﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class OperationManager : Singleton<OperationManager> 
{
	public OperationType operationType;

	public override void Start ()
	{
		base.Start();
	}

	private void SetOperation(OperationType OT)
	{
		//InputManager.Instance.primaryAction.onDown = SetType;
		operationType=OT;
	}

	private void SetType(params float[] dims)
	{
		OperationFactory.MakeOperation(operationType);
	}

	public void SetMoveOperation()
	{
		SetOperation(OperationType.Move); 
	}

	public void SetRotateOperation()
	{
		SetOperation(OperationType.Rotate);
	}

	public void SetScaleOperation()
	{
		SetOperation(OperationType.Scale);
	}

	public void SetConnectOperation()
	{
		SetOperation(OperationType.Connect);
	}

	public void SetWeldOperation()
	{
		SetOperation(OperationType.Weld);
	}

	public void SetSmoothOperation()
	{
		SetOperation(OperationType.Smooth);
	}

	public void SetDeleteOperation()
	{
		SetOperation(OperationType.Delete);
	}

	public void SetMakeFaceOperation()
	{
		SetOperation(OperationType.MakeFace);
	}

	public void SetExtrudeOperation()
	{
		SetOperation(OperationType.Extrude);
	}

	public void SetSubdivOperation()
	{
		SetOperation(OperationType.Subdiv);
	}

	public void SetFlipOperation()
	{
		SetOperation(OperationType.Flip);
	}

	public void LoadSceneName(string name)
	{
		SceneManager.LoadScene(name);
	}
}
