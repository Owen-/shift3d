﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RotateOperation : TransformPointsOperation 
{
	private Vector3 average;

	public RotateOperation() : base()
	{
		average = Function.GetCentrePoint(points);
	}

	public override void VisualUpdate(params float[] dims)
	{
		if(dims.Length!=2) return;
		foreach (Point P in points) Rotate(P, dims);
		base.VisualUpdate(dims);
	}


	private void Rotate(Point P, float[] dims)
	{
		Vector3 dir = operationDirection;

		if (DirectionManager.Instance.directionType == DirectionManager.DirectionType.Free) 
		{
			dir=Camera.main.transform.rotation *dir; //Dir is just the vector3 in 2D space, ie z direction is always 0 even when rotated
		}
		P.VisualMove (-average); 
		P.VisualSet  (Quaternion.AngleAxis(dims[0], dir) * P.loc);
		P.VisualMove(average);
	}

}
