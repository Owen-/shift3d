using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MoveOperation : TransformPointsOperation 
{
	public override void VisualUpdate(params float[] dims)
	{
		if(dims.Length!=2) return;
		foreach (Point P in points) P.VisualMove(operationDirection*dims[0]);
	}
}
