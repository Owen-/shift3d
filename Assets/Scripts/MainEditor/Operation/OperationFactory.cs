﻿using UnityEngine;
using System.Collections;

public class OperationFactory
{
	public static Operation MakeOperation(OperationType OT)
	{
		switch (OT)
		{
			case OperationType.Move:
				return new MoveOperation() as Operation;
			case OperationType.Scale:
				return new ScaleOperation() as Operation;
			case OperationType.Rotate:
				return new RotateOperation() as Operation;
			case OperationType.Connect:
				return new ConnectOperation() as Operation;
			case OperationType.Weld:
				return new WeldOperation() as Operation;
			case OperationType.Smooth:
				return new SmoothOperation() as Operation;
			case OperationType.Delete:
				return new DeleteOperation() as Operation;
			case OperationType.MakeFace:
				return new MakeFaceOperation() as Operation;
			case OperationType.Extrude:
				return new ExtrudeOperation() as Operation;
			case OperationType.Subdiv:
				return new SubdivOperation() as Operation;
			case OperationType.Flip:
				return new FlipOperation() as Operation;
		}

		return null;
	}
}
