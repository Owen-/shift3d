﻿using System.Collections;

public enum OperationType 
{
	Move, Rotate, Scale, Connect, Weld, Smooth, Delete, MakeFace, Extrude, Subdiv, Flip
}