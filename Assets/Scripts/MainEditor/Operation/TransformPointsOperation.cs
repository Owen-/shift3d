﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public abstract class TransformPointsOperation : Operation 
{
	protected List<Point> points;
	protected List<Vector3> initialLocation;
	protected List<Vector3> finalLocation;

	public TransformPointsOperation() : base()
	{
		points = new List<Point>(SelectionManager.Instance.GetSelectedAsPoints());
		initialLocation = points.Select(o => o.loc).ToList();
		finalLocation = new List<Vector3>();

	}

	public override void RegisterEndState(params float[] dims)
	{
		
		if(dims.Length!=2) return;

		foreach (Point P in points)
		{
			finalLocation.Add(P.loc);
		}

		VisualUpdate(dims);

		Perform();
	}

	public override void VisualUpdate(params float[] dims)
	{
		foreach (Point P in points)
		{
			foreach (Face F in P.GetFaces()) F.Build(); // this is building things a bunch of times fix itttt
		}
	}

	private void Reset()
	{
		for(int i =0; i<points.Count; i++)
		{
			points[i].transform.position = initialLocation[i];
		}
	}
		
	public override void Perform()
	{
		for(int i = 0; i<points.Count; i++)
		{
			points[i].loc = finalLocation[i];
		}
	}
}
