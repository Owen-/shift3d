﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Start and Destroy functions;
public abstract class Selectable : MonoBehaviour 
{
	protected bool isSelected = false;
	public virtual void SetSelected(bool b, List<Selectable> sels)
	{
		isSelected = b;

		if (b)
		{
			if (!sels.Contains(this as Selectable))
			{
				sels.Add(this);
			}
		}

		else
		{
			if (sels.Contains(this as Selectable)) 
			{
				sels.Remove(this);
			}
		}
	}

	public abstract List<Selectable> As<T>() where T : Selectable;

	public bool GetSelected()
	{
		return isSelected;
	}

	//should we have an abs fn that is like "GetPoints()"?

}
