using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Point : Selectable
{
	//private Vector3 loc;
	//private Vector3 norm;
	//private Vector2 texLoc;
	[SerializeField] private float pointSize = 0.003f;
	[SerializeField] private MeshRenderer MR;

	private List<Face> faces;
	private List<Edge> edges;

	public Vector3 loc
	{
		get
		{
			return transform.position;
		}
		set
		{ 
			transform.position=value;
		}
	}

	private void Awake()
	{
		edges = new List<Edge>();		
		faces=new List<Face>();
		MR=GetComponent<MeshRenderer>();
	}

	private void Update()
	{
		float size = (Camera.main.transform.position - transform.position).magnitude;
		float camFactor = Camera.main.orthographicSize;
	 	transform.localScale = new Vector3(size,size,size) * pointSize * camFactor;
	}

	public override void SetSelected(bool b, List<Selectable> sels)
	{
		base.SetSelected(b, sels);
		MR.enabled = b;
	}

	public void VisualMove(Vector3 v)
	{
		transform.position+=v;
	}

	public void VisualSet(Vector3 v)
	{
		transform.position=v;
	}

	public bool IsConnectedTo(Point P)
	{
		foreach(Edge E in edges)
		{
			if(E.ContainsPoint(P)) return true; 
		}
		return false;
	}

	public Edge GetEdgeWith(Point P)
	{
		foreach (Edge E in edges)
		{
			if(E.ContainsPoint(P)) return E; 
		}
		return null;
	}

	public List<Face> GetFaces() 	{ return faces; }
	public List<Edge> GetEdges() 	{ return edges; }

	public void AddFace(Face F) 	{ if(!faces.Contains(F)) faces.Add(F); }
	public void AddEdge(Edge E)		{ if(!edges.Contains(E)) edges.Add(E); }

	public void RemoveFace(Face F) 	{ faces.Remove(F); }
	public void RemoveEdge(Edge E) 	{ edges.Remove(E); }

	public bool IsAssociatedSelected()
	{
		if (SelectionManager.Instance.selMode==SelMode.Point) 
		{
			return GetSelected();
		}

		if (SelectionManager.Instance.selMode==SelMode.Edge) 
		{
			foreach (Edge E in edges)
			{
				if (E.GetSelected()) return true;
			}
			return false;
		}

		if (SelectionManager.Instance.selMode==SelMode.Face)
		{
			foreach (Face F in faces)
			{
				if (F.GetSelected()) return true;
			}
			return false;
		}

		return false;
	}

	public override List<Selectable> As<T>()
	{
		List<Selectable> result = new List<Selectable>();
		if (typeof(T) == typeof(Point)) 
		{
			result.Add(this);
			return result;
		}
		else if (typeof(T) == typeof(Edge)) return edges.ConvertAll(x => (Selectable)x);
		else if (typeof(T) == typeof(Face)) return faces.ConvertAll(x => (Selectable)x);
		else Debug.Log("Type not supported");
		return result;

	}

}
