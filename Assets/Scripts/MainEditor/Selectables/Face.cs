using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//should calcnorm be here? I mean probably

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshCollider))]
public class Face : Selectable
{
	[SerializeField] private List<Point> points;
	private List<Edge> edges;

	private MeshFilter MF;
	private MeshRenderer MR;
	private MeshCollider MC;
	private Vector3 norm;

	private void Awake()
	{
		MF = this.gameObject.GetComponent<MeshFilter>();
		MR = this.gameObject.GetComponent<MeshRenderer>();
		MC = this.gameObject.GetComponent<MeshCollider>();

		points = new List<Point>();
		edges = new List<Edge>();
	}

	// Check to see if the point's list of edges already includes the edge you are trying to make, if so just add this face to the edge's neighbourFaces
	public void MakeEdges(GameObject GO)
	{
		for (int i = 0; i<points.Count; i++)
		{
			int second = i+1;
			if (i==points.Count-1) second = 0;

			if(!points[i].IsConnectedTo(points[second]))
			{
				Edge E = (Instantiate(Resources.Load("Edge")) as GameObject).GetComponent<Edge>();
				E.transform.parent=GO.transform;
				E.SetPoints(points[i], points[second]);
				AddEdge(E);
			}
			else
			{
				Edge E = points[i].GetEdgeWith(points[second]);
				AddEdge(E);
			}
		}
	}

	//should have another build function with a param list pf points to build a face
	public void Build()
	{
		//if built rebuild
		norm = CalcNorm();
		MF.mesh.vertices = GetPointLocations().ToArray();
		MF.mesh.triangles = TopologyGenerator3D.GenerateTris(GetPointLocations());
		MF.mesh.RecalculateBounds();
		MC.sharedMesh = null;
		MC.sharedMesh = MF.mesh;
	}

	//Calculates the average normal of the face
	private Vector3 CalcNorm()
	{
		Vector3 dir = new Vector3(0f,0f,0f);
        for (int i = 0; i<points.Count; i++)
        {
            if (i+2 < points.Count) 		dir+= Vector3.Normalize(Vector3.Cross(points[i+1].transform.position - points[i].transform.position, points[i+2].transform.position - points[i].transform.position));
            else if (i+2 == points.Count) 	dir+= Vector3.Normalize(Vector3.Cross(points[i+1].transform.position - points[i].transform.position, points[0].transform.position - points[i].transform.position));
            else if (i+1 == points.Count) 	dir+= Vector3.Normalize(Vector3.Cross(points[0].transform.position - points[i].transform.position, points[1].transform.position - points[i].transform.position));
        }
        dir.Normalize();
        return dir;
	}

	public List<Vector3> GetPointLocations()
	{
		List<Vector3> vs = new List<Vector3>();
		for (int i = 0; i< points.Count; i++) 
		{
			vs.Add(points[i].transform.position);
		}
		return vs;
	}

	public Selectable AsType(SelMode s, Vector3 pos)
	{
		if (s == SelMode.Face) return this;
		if (s == SelMode.Edge) return Function.ClosestEdge(pos, edges);
		if (s == SelMode.Point) return Function.ClosestPoint(pos, points);

		return null;
	}
	
	public List<Point> GetPoints() 		{ return points; }
	public List<Edge> GetEdges() 		{ return edges;	}

	public void AddPoint(Point P)		{ if(!points.Contains(P)) points.Add(P); }
	public void AddEdge(Edge E)			{ if(!edges.Contains(E)) edges.Add(E); }
	
	public void RemovePoint(Point P)	{ points.Remove(P); }
	public void RemoveEdge(Edge E)		{ edges.Remove(E); }

	public override void SetSelected(bool b, List<Selectable> sels) 
	{
		base.SetSelected(b, sels);

		if (b) 
			MR.material.color = Color.red;
		else 
			MR.material.color = Color.white; 
	}

	public override List<Selectable> As<T>()
	{
		List<Selectable> result = new List<Selectable>();
		if (typeof(T) == typeof(Face)) 
		{
			result.Add(this);
			return result;
		}
		else if (typeof(T) == typeof(Edge)) return edges.ConvertAll(x => (Selectable)x);
		else if (typeof(T) == typeof(Point)) return points.ConvertAll(x => (Selectable)x);
		else Debug.Log("Type not supported");
		return result;

	}
		
	public void Move(Vector3 v) {}
	public void Rotate() {}
	public void Scale(){}

}
