﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Edge : Selectable
{
	private List<Face> faces;
	private Point pointA;
	private Point pointB;
	public Color currentColour = Color.black;

	private Material lineMaterial;

	public void SetPoints(Point A, Point B)
	{
		pointA = A;
		pointB = B;

		pointA.AddEdge(this);
		pointB.AddEdge(this);
	}

	public Point GetPointA()
	{
		return pointA;
	}

	public Point GetPointB()
	{
		return pointB;
	}

	public override void SetSelected(bool b, List<Selectable> sels)
	{
		base.SetSelected(b, sels);

		if(b) 
			currentColour = Color.red;
		else 
			currentColour = Color.black; 
	}

	public void Move(Vector3 v)
	{

	}

	public void Rotate()
	{

	}

	public void Scale()
	{
		
	}

	public bool ContainsPoint(Point P)
	{
		if (pointA == P || pointB == P) return true;

		return false;
	}

	 private void OnRenderObject()
	{
		Vector3 camV = Camera.main.transform.forward;
		camV=Vector3.Normalize(camV)/50; 

		lineMaterial.SetPass(0);
	    GL.PushMatrix(); 
	    GL.Begin(GL.LINES);
	    GL.Color(currentColour);
		GL.Vertex(pointA.transform.position - camV);
		GL.Vertex(pointB.transform.position - camV);
		GL.End();
		GL.PopMatrix();		
	}  

	private void Start()
	{
		lineMaterial = new Material(Shader.Find("S3DLines")); 
		lineMaterial.hideFlags = HideFlags.HideAndDontSave;
		lineMaterial.shader.hideFlags = HideFlags.HideAndDontSave;
		currentColour=Color.black;
		GeometryManager.Instance.edges.Add(this);
	}

	public void Destroy()
	{
		GeometryManager.Instance.edges.Remove(this);
		Destroy(this.gameObject);
	}

	public override List<Selectable> As<T>()
	{
	// 	return _as<T>();
	// }

	// private List<Selectable> _as<T>() where T : Selectable
	// {
		List<Selectable> result= new List<Selectable>();
		if (typeof(T) == typeof(Edge)) 
		{
			result.Add(this);
			return result;
		}
		else if (typeof(T) == typeof(Face))
		{
			return faces.ConvertAll(x=> (Selectable)x);
		}
		else if (typeof(T) == typeof(Point)) 
		{
			result.Add(pointA);
			result.Add(pointB);
			return result;
		}
		else Debug.Log("Type not supported");
		return new List<Selectable>();
	}

}
