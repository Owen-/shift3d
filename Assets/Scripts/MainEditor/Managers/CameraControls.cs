using UnityEngine;
using System.Collections;

//still needs focus camera operations
//    which still require update
//still needs snapping to view

public class CameraControls : Singleton<CameraControls> 
{
	public float panSensitivity = 0.01f;
	public float zoomSensitivity = 1f;
	public float rotateSensitivity = 0.01f;

	private Vector3 focusPoint;
	private Camera cam;
 
	public override void Start()
	{
		base.Start();
		focusPoint = new Vector3(0f,0f,0f);
		cam = Camera.main;
	}

	private void Update()
	{
		ZoomAndPan();
		cam.transform.LookAt(focusPoint);
	}

	private void ZoomAndPan()
	{
		#if UNITY_EDITOR // quick fix to give me access to testing these controls in the
			if(Input.GetKey(KeyCode.Mouse1)) 
				Pan(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

			if(Input.GetKey(KeyCode.Mouse2)) 
				Zoom(Input.mouseScrollDelta.y);
		#endif

		#if UNITY_ANDROID
			if (Input.touchCount == 2) 
				ZoomAndPan(Input.touches[0].position, Input.touches[1].position);
		#endif
	}

	private void Pan(float x, float y)
	{
		Vector3 panAmount = (cam.transform.right*x + cam.transform.up*y) * panSensitivity;
		
		cam.transform.position += panAmount;
		focusPoint += panAmount;		
	}

	private void Zoom(float x)
	{
		float zoomAmount = x * zoomSensitivity;

		if (cam.orthographic) 
			cam.orthographicSize -= zoomAmount;
		else 
			cam.fieldOfView -= zoomAmount;
	}

	public void Rotate(float x, float y)
	{
		x*=rotateSensitivity;
		y*=rotateSensitivity;
		cam.transform.RotateAround(focusPoint, cam.transform.up, x);
		cam.transform.RotateAround(focusPoint, cam.transform.right, -y);
		cam.transform.eulerAngles =  new Vector3(cam.transform.eulerAngles.x, cam.transform.eulerAngles.y, 0f); // ensures no z rotation
	}


	

	public void SetRotate()
	{
		TouchInputManager.Instance.overModelController.Initialize  (null, Rotate, null, TouchInputController.Type.Directional);
		TouchInputManager.Instance.backgroundController.Initialize (null, Rotate, null, TouchInputController.Type.Directional);
	}

	public void ToggleOrtho() 
	{
		cam.orthographic = !cam.orthographic;
	}

	public void FitCam()
	{
		FocusCamera(Function.GetCentrePoint(SelectionManager.Instance.GetSelectedAsPoints()));

		//if (cam.orthographic) cam.orthographicSize = 
		//else cam.fieldOfView = ;
	}

	public void FocusCamera()
	{
		FocusCamera(Function.GetCentrePoint(SelectionManager.Instance.GetSelectedAsPoints()));
	}


	private void FocusCamera(Vector3 p)
	{
		focusPoint = p;
	}




	public Vector3 GetFreeDirection()
	{
		return new Vector3(0f,0f,0f);
	}

}
