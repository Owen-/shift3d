﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GeometryManager : Singleton<GeometryManager> 
{
	public List<string> comments;
	public List<Face> faces;
	public List<Point> points;
	public List<Edge> edges;

	// deconstructs the relevant lines into more manageable data
	public void BuildObject(string[] lines)
	{
		faces = new List<Face> ();
		points = new List<Point> ();
		edges = new List<Edge>();

		lines = TrimComments(lines);
		MakePoints(lines);
		MakeFaces(lines);
		MakeEdges();

		RenderObject();
	}

	private void RenderObject()
	{
		foreach (Face F in faces) F.Build();
	}

	//removes comments to stop them interfering with processing each line
	private string[] TrimComments(string[] lines)
	{
		for (int i = 0; i< lines.Length; i++)
		{
			if (lines[i].Contains("#")) 
			{
				lines[i] = lines[i].Split('#')[0];
			}
		}
		return lines;
	}

	//This function removes any normal and texture applied to the point for the sake of speeding up the loading time
	private void MakePoints(string[] lines)
	{
		GameObject GO = new GameObject("Points");
		foreach (string line in lines)
		{
			if (line.StartsWith("v "))
			{
				Point P = (Instantiate(Resources.Load("Point")) as GameObject).GetComponent<Point>();

				string[] subdata=line.Split(' ');
				P.transform.position = new Vector3(System.Single.Parse(subdata[1]), System.Single.Parse(subdata[2]), System.Single.Parse(subdata[3]));
				P.transform.parent=GO.transform;
				points.Add(P);
			}
		}
	}

	//Makes each face and ensure *all* points have the correct corresponding face
	private void MakeFaces(string[] lines)
	{
		GameObject GO = new GameObject("Faces");
		foreach (string line in lines)
		{
			if (line.StartsWith("f "))
			{
				Face F = (Instantiate(Resources.Load("Face")) as GameObject).GetComponent<Face>();
				string[] subdata=line.Split(' ');

				foreach (string sl in subdata) 
				{
					string[] divisions= sl.Split('/');
					if (divisions.Length > 0 && !divisions[0].StartsWith("f"))
					{
						Point P = points[(int)System.Single.Parse(divisions[0])-1];
						P.AddFace(F);
						F.AddPoint(P);
					}
				}
				faces.Add(F);
				F.transform.parent = GO.transform;
			}
		}
	}

	private void MakeEdges()
	{
		GameObject GO = new GameObject("Edges");
		foreach (Face F in faces)
		{
			F.MakeEdges(GO);
		}
	}
}
