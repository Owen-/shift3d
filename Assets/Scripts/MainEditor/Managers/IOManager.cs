using System.IO;
using System.Linq;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class IOManager : Singleton<IOManager> 
{
	private string fileData;
	private byte[] imageData;

	public void LoadFile(string name)
	{
		StartCoroutine(LoadFileNextFrame(name));
	}

	public IEnumerator LoadFileNextFrame(string name)
	{
		yield return null; 	//Allows MainMenuManager time to delete itself
		if(name!="")
		{
			if (File.Exists(Application.persistentDataPath + "/" + name))
				fileData = File.ReadAllText(Application.persistentDataPath + "/" + name);

			else if (File.Exists(Application.dataPath + "/Resources/Primatives/" + name))
				fileData = File.ReadAllText(Application.dataPath + "/Resources/Primatives/" + name);

			if (fileData!=null)
			{
				string[] data= fileData.Split('\n');
				GeometryManager.Instance.BuildObject(data);
			}
				
		}
		else Debug.Log("no file specified!");
	}



	 public void SaveObject(string fileName)
	 {
		GeometryManager GM = GeometryManager.Instance;
	 	fileData="";
	 	foreach (string comment in GM.comments) fileData+="# " + comment + "\n";
	 	fileData+="mtllib "+fileName+".mtl\no " + fileName + "\n";

	 	List<Vector3> vs = new List<Vector3>();

	 	foreach (Face f in GM.faces) 
	 	{
			foreach (Point p in f.GetPoints())
	 		{
				vs.Add(p.gameObject.transform.position);
	 		}
	 	}

	 	vs = vs.Distinct().ToList();

	 	foreach (Vector3 v in vs)  fileData+="v " + v.x + " " + v.y + " " + v.z + "\n"; 

	 	fileData+="usemtl "+fileName+"\ns off\n";
	
	 	foreach (Face f in GM.faces) 
	 	{
	 		string fdata = "";
	 		int vInd =0;

			for (int i =0; i<f.GetPoints().Count; i++)
	 		{
				for (int j=0; j<vs.Count;  j++) if (f.GetPoints()[i].gameObject.transform.position 	== vs[j]) 	vInd =j+1;
	 			fdata += " " + vInd;

	 		}
	 		fileData+="\nf" + fdata;
	 	}
	 	fileData+="\n";

		File.WriteAllText(Application.persistentDataPath +"/"+ fileName + ".obj" , fileData);
	 }
}

















//Useful for when I try to store texture data
//if(File.Exists(Application.persistentDataPath + "/" + name.Replace(".obj",".png")))
//{
//	imageData = File.ReadAllBytes(Application.persistentDataPath + "/" + name.Replace(".obj",".png"));
//	GM.modelTex.LoadImage(imageData);
//}