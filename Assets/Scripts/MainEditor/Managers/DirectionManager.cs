﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DirectionManager : Singleton<DirectionManager> 
{
	public Image X_image, Y_image, Z_image, Free_image, Normal_image;
	public enum DirectionType { None, Free, X, Y, Z, Normal };
	public DirectionType directionType;

	public Vector3 GetDirection()
	{
		switch (directionType)
		{
			case DirectionType.Free:
				return GetFreeDirection();
			case DirectionType.Normal:
				return GetNormalDirection();
			case DirectionType.X:
				return GetXDirection();
			case DirectionType.Y:
				return GetYDirection();
			case DirectionType.Z:
				return GetZDirection();
			default:
				return GetFreeDirection();
		}
	}

	private Vector3 GetXDirection()
	{
		return new Vector3(1f, 0f, 0f);
	}

	private Vector3 GetYDirection()
	{
		return new Vector3(0f, 1f, 0f);
	}

	private Vector3 GetZDirection()
	{
		return new Vector3(0f, 0f, 1f);
	}

	private Vector3 GetNormalDirection()
	{
		return SelectionManager.Instance.GetSelectedNormal();
	}

	private Vector3 GetFreeDirection()
	{
		return CameraControls.Instance.GetFreeDirection();
	}

	public void SetXDirection()
	{
		directionType=DirectionType.X;
		SetButtonColour();
	}

	public void SetYDirection()
	{
		directionType=DirectionType.Y;
		SetButtonColour();
	}

	public void SetZDirection()
	{
		directionType=DirectionType.Z;
		SetButtonColour();
	}

	public void SetFreeDirection()
	{
		directionType=DirectionType.Free;
		SetButtonColour();
	}

	public void SetNormalDirection()
	{
		directionType=DirectionType.Normal;
		SetButtonColour();
	}

	public void SetButtonColour()
	{
		X_image.color = (directionType!=DirectionType.X) ? Color.white : new Color(128f/255f, 149f/255f, 1);
		Y_image.color = (directionType!=DirectionType.Y) ? Color.white : new Color(128f/255f, 149f/255f, 1);
		Z_image.color = (directionType!=DirectionType.Z) ? Color.white : new Color(128f/255f, 149f/255f, 1);
		Free_image.color = (directionType!=DirectionType.Free) ? Color.white : new Color(128f/255f, 149f/255f, 1);
		Normal_image.color = (directionType!=DirectionType.Normal) ? Color.white : new Color(128f/255f, 149f/255f, 1);
	}
}