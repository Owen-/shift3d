using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SelectionManager : Singleton<SelectionManager> 
{
	public enum IterMode {Additive, Subtractive, None}
	public IterMode iterMode = IterMode.None;
	public SelMode selMode = SelMode.None;

	public List<Selectable> selected;

	public override void Start ()
	{
		base.Start();
		selected = new List<Selectable>();
	}

	public void SetFaceSelect()
	{
		SetSelectType(SelMode.Face);
	}

	public void SetEdgeSelect()
	{
		SetSelectType(SelMode.Edge);
	}

	public void SetPointSelect()
	{
		SetSelectType(SelMode.Point);
	}

	private void SetSelectType(SelMode s)
	{
		if(selMode == s) InvertSelection();
		else ClearSelection();

		selMode = s;
		//InputManager.Instance.primaryAction.onDown = MakeInitialSelection;
		//InputManager.Instance.primaryAction.onHold = MakeSelection;
		//InputManager.Instance.primaryAction.SetIsDelta(false);
	}

	// is primary needs to be set
	// need to be able to setSelected without checking every frame or indeed repeatedly. 
	private void MakeSelection(params float[] dims)
	{
		if (dims.Length != 2) return;

		Selectable hitSel = RaycastManager.GetTouchedSelectable(dims[0], dims[1], selMode);
		if (hitSel != null) ProcessSelected(hitSel);
	}

	private void MakeInitialSelection(params float[] dims)
	{
		if (dims.Length != 2) return;

		Selectable hitSel = RaycastManager.GetTouchedSelectable(dims[0], dims[1], selMode);
		if (hitSel != null) 
		{
			if (hitSel.GetSelected())
			{
				hitSel.SetSelected(false, selected);
				iterMode=IterMode.Subtractive;
			}

			else 
			{
				hitSel.SetSelected(true, selected);
				iterMode=IterMode.Additive;
			}
		}
	}

	public void InvertSelection()
	{
		if (selMode == SelMode.Point)
			foreach (Point P in GeometryManager.Instance.points)
				P.SetSelected(!P.GetSelected(), selected);

		if (selMode == SelMode.Edge)
			foreach (Edge E in GeometryManager.Instance.edges)
				E.SetSelected(!E.GetSelected(), selected);
		
		if (selMode == SelMode.Face)
			foreach (Face F in GeometryManager.Instance.faces)
				F.SetSelected(!F.GetSelected(), selected);
	}

	public void ClearSelection()
	{
		for(int i = 0; i<selected.Count; i++) 
		{
			selected[i].SetSelected(false, selected);
			ClearSelection();
		}
	}

	private void ProcessSelected(Selectable sel)
	{
		for(int i =0; i<selected.Count; i++)
		{
			if(sel == selected[i])
			{
				if (iterMode == IterMode.Subtractive) 
				{
					sel.SetSelected(false, selected);
				}
				return;
			}
		}

		if (iterMode == IterMode.Additive) 
		{
			sel.SetSelected(true, selected); //why is this getting fucked sometimes
		}
		return;
	}

	public Vector3 GetSelectedNormal()
	{
		return new Vector3(0f,0f,0f);
	}

	public List<Point> GetSelectedAsPoints()
	{
		List<Point> res = new List<Point>();
		for (int i = 0; i<selected.Count; i++)
		{
			if ( selected[i] as Point !=null) res.Add(selected[i] as Point);
			else if ( selected[i] as Edge != null) 
			{
				Point a = (selected[i] as Edge).GetPointA();
				Point b = (selected[i] as Edge).GetPointB();

				if(!res.Contains(a)) res.Add(a);
				if(!res.Contains(b)) res.Add(b);
			}
			else if ( selected[i] as Face !=null)
			{
				foreach (Point p in (selected[i] as Face).GetPoints())
				{
					if(!res.Contains(p)) res.Add(p);
				}
			}
		}
		return res;
	}

	public List<T> GetSelectedAsType<T>() where T : Selectable
	{
		List<T> result = new List<T>();
		for (int i = 0; i<selected.Count; i++)
		{
			for (int j = 0; j<selected[i].As<T>().Count; j++)
			{
				if(!result.Contains(selected[i].As<T>()[j] as T)) result.Add(selected[i].As<T>()[j] as T);
			}


			// if ( selected[i] as T !=null) res.Add(selected[i] as T);

			// else if ( selected[i] as Edge != null) 
			// {
			// 	Point a = (selected[i] as Edge).GetPointA();
			// 	Point b = (selected[i] as Edge).GetPointB();

			// 	if(!res.Contains(a)) res.Add(a);
			// 	if(!res.Contains(b)) res.Add(b);
			// }

			// else if ( selected[i] as Face !=null)
			// {
			// 	foreach (Point p in (selected[i] as Face).GetPoints())
			// 	{
			// 		if(!res.Contains(p)) res.Add(p);
			// 	}
			// }
		}
		return result;
	}


	
}




