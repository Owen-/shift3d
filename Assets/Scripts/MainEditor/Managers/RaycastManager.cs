using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RaycastManager : Singleton<RaycastManager> 
{
	public static bool isOverUI
	{
		get 
		{
			if (SystemInfo.isRunningOnAndroid)
			{
				Touch T = Input.GetTouch(0);
				return UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(T.fingerId);
			}

			else
			{
				return UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(-1);
			}
		}
	}

	public static bool isCursorOnObject 
	{
		get
		{
			return (S3DRaycastHit(0f,0f).collider != null);
		}
	}

	public static Selectable GetTouchedSelectable(float h, float v, SelMode selMode)
	{
		RaycastHit RH = S3DRaycastHit(h, v);
		if(RH.collider==null) return null;

		Face F = RH.collider.gameObject.GetComponent<Face>();
        if (F == null) return null;

		Selectable hitSel = F.AsType(selMode, RH.point); 

		return hitSel;
	}	

	public static RaycastHit S3DRaycastHit(float h, float v)
	{
	    RaycastHit hit;
		Vector3 mousePos = Camera.main.ScreenToWorldPoint(new Vector3(h, v, Camera.main.nearClipPlane));
	    
	    if(!Camera.main.orthographic) 
	        Physics.Raycast(mousePos, mousePos-Camera.main.transform.position, out hit);
	    else 
	        Physics.Raycast(mousePos, Camera.main.transform.forward, out hit);

        return hit; 
	}
}
