using UnityEngine;
using System.Collections;

//merge with operation manger, should be called operation manager
public class TouchInputManager : Singleton<TouchInputManager> 
{
	public TouchInputController overModelController;
	public TouchInputController backgroundController;

	public override void Start()
	{
		base.Start();

		overModelController = new TouchInputController();
		backgroundController= new TouchInputController();
	}

	private void Update()
	{
		if(RaycastManager.isCursorOnObject) 
			overModelController.TryPerformOperation();
		else
			backgroundController.TryPerformOperation();
	}

}