﻿using System.IO;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class MainMenuManager : Singleton<MainMenuManager> 
{
	public GameObject localObjectsPanel;
	public GameObject noLocalObjAlert;
	public Transform primativesSlider;
	public float sliderSensitivity;
	public float checkFileRate;

	private bool canSlide = true;
	private List<GameObject> localObjects;


	private float halfCamera 
	{
		get { return Camera.main.orthographicSize * 0.5f; }
	}

	public override void Start()
	{
		base.Start();
		localObjects = new List<GameObject>();
      	InvokeRepeating("CheckFilesInPath", 0f, checkFileRate);
      	
		primativesSlider.position = new Vector3( 3f*primativesSlider.localScale.x - Camera.main.ViewportToWorldPoint(Vector3.one).x, halfCamera, 0f);
   }

	private void Update()
	{
    	MovePrefabSlider(HorizontalSwipeDelta());
   	}

	public void MovePrefabSlider(float amount)
	{
      	amount*=sliderSensitivity;
      
      	float max = Camera.main.ViewportToWorldPoint(Vector3.one).x;
		float width = (primativesSlider.childCount-1)*5f*primativesSlider.localScale.x;
		float newX = Mathf.Clamp(primativesSlider.position.x+amount, max-width-(3f*primativesSlider.localScale.x)  , (3f*primativesSlider.localScale.x) - max);
      
		primativesSlider.position = new Vector3(newX, halfCamera, 0f);                        
   }

	private float HorizontalSwipeDelta()
	{
		float dH = 0f;

		if(Input.touchCount > 0 && SystemInfo.isRunningOnAndroid)
    	{
         	if(Input.touches[0].position.y < Screen.height/2) return 0f;
         	dH=Input.touches[0].deltaPosition.x;
      	}

		if(!SystemInfo.isRunningOnAndroid && Input.GetKey(KeyCode.Mouse0))
      	{
         	if ( Input.mousePosition.y < Screen.height/2) return 0f;
         	dH = Input.GetAxis("Mouse X");         
      	}

		return dH;
	}

   	public void SetSwipeState(bool b) 
	{ 
		canSlide = b; 
	}

   	private void CheckFilesInPath()
   	{
        DirectoryInfo dir = new DirectoryInfo(Application.persistentDataPath);
        FileInfo[] info = dir.GetFiles("*.obj*");
   		string[] files = new string[info.Length];
         
        for(int i =0; i<info.Length; i++) files[i]=(info[i].Name);

        if (info.Length == 0) noLocalObjAlert.SetActive(true);
        else if (localObjects.Count != info.Length) DisplayLocalObjects(files);
   	}

   	private void DisplayLocalObjects(string[] files)
   	{
        if (localObjects.Count != 0) 
		{
			foreach (GameObject go in localObjects) 
			{
				Destroy(go);
			}
		}

        noLocalObjAlert.SetActive(false);
   		for(int i = 0; i< files.Length; i++)
   		{
			GameObject LOP = Instantiate(Resources.Load("MainMenu/LocalObjectPrefab")) as GameObject;
			LOP.transform.SetParent(localObjectsPanel.transform);
   			LOP.transform.GetChild(0).gameObject.GetComponent<Text>().text=files[i];
   			localObjects.Add(LOP);
   		}
   	}

	public void LoadObject(string objectName)
	{
		CancelInvoke();
 		SceneManager.LoadScene("Editor");
		IOManager.Instance.LoadFile(objectName);
		Destroy(this.gameObject);
   	}

	public bool CanSlide()
	{
		return canSlide;
	}
		
}
