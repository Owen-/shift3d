﻿using UnityEngine;
using System.Collections;

public class Splash : Singleton<Splash>
{
	public void Hide()
	{
		Instance.gameObject.SetActive(false);
	}
}
