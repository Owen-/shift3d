﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LocalObjectSelector : MonoBehaviour 
{
	public void LoadScene()
	{
		MainMenuManager.Instance.LoadObject(this.transform.GetChild(0).gameObject.GetComponent<Text>().text);
	}
}
