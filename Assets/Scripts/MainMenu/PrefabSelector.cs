﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// Determines wether user is swiping to scroll through prefabs or tapping to select them

public class PrefabSelector : MonoBehaviour 
{
	public float longTapTimer;
	public string objectName;

	private MainMenuManager mainMenuManager;
	private bool isLong = false;

	private void Start()
	{
		mainMenuManager = MainMenuManager.Instance;
	}

	private void OnMouseDown()
	{
		if(mainMenuManager.CanSlide())
		{
			isLong=false;
			Invoke("isTapLong", longTapTimer);
		}
	}

	private void OnMouseUp()
	{
		if (!isLong && mainMenuManager.CanSlide()) mainMenuManager.LoadObject(objectName);		
	}

	private void isTapLong()
	{
		isLong = true;
	}
}
