﻿Shader "S3DLines"
{
    SubShader 
    {
        Tags { Queue = Transparent }
        Pass 
        {
        	Blend SrcAlpha OneMinusSrcAlpha
            ZWrite On
            ZTest LEqual
	        BindChannels 
	        { 
        		Bind "vertex", vertex Bind "color", color
    		}
        }
    }
}