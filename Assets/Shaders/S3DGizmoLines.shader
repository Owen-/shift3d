﻿Shader "S3DGizmoLines"
{
    SubShader 
    {
        Tags { "RenderType"="Opaque" }
        Pass 
        {
            ZWrite On
            ZTest Always
	        BindChannels 
	        {
        		Bind "vertex", vertex Bind "color", color
    		}
        }
    }
}